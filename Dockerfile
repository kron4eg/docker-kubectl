FROM alpine:3.7 as builder

ENV KUBECTL_ARCHIVE=kubernetes-client-linux-amd64.tar.gz
ENV KUBECTL_VERSION=v1.9.2
ENV KUBECTL_SHA265=c45cf9e9d27b9d1bfc6d26f86856271fec6f8e7007f014597d27668f72f8c349
ENV KUBECTL_URL=https://dl.k8s.io/${KUBECTL_VERSION}/${KUBECTL_ARCHIVE}
ENV KONTEXT_VERSION=v0.2.1
ENV KONTEXT_URL=https://gitlab.com/kron4eg/kontext/-/jobs/artifacts/${KONTEXT_VERSION}/download?job=release-linux

WORKDIR /tmp
ADD ${KUBECTL_URL} .
RUN echo "${KUBECTL_SHA265} *${KUBECTL_ARCHIVE}" > ${KUBECTL_ARCHIVE}.sha256
RUN apk --no-cache add perl-utils
RUN shasum -a 256 -c ${KUBECTL_ARCHIVE}.sha256
RUN tar xvf ${KUBECTL_ARCHIVE}
ADD ${KONTEXT_URL} kontext_${KONTEXT_VERSION}.zip
RUN unzip kontext_${KONTEXT_VERSION}.zip

# result image
FROM docker:stable

COPY --from=builder /tmp/kubernetes/client/bin/kubectl /usr/local/bin/
COPY --from=builder /tmp/kontext /usr/local/bin/
